import os


workers = os.environ.get('GUNICORN_WORKERS', 4)
bind = os.environ.get('BIND_ADDRESS', '0.0.0.0:80')
