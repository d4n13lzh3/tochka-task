import aiomisc
import logging

from aiomisc.log import basic_config
from asyncpgsa import pg

from account.collector import AccountBalanceCollector
from account.settings.config import load_collector_config


logger = logging.getLogger(__name__)


def setup_logging():
    basic_config(buffered=False)


def main():
    setup_logging()
    logger.info('Load config from environments...')
    config = load_collector_config()
    logger.info('Initialize collector...')
    service = AccountBalanceCollector(
        pg=pg,
        interval=config.interval,
        delay=config.delay,
        # TODO: Отключим оценочное число записей, в
        #  дальнейшем надо сделать включение умным
        estimate_counting=False,
    )

    with aiomisc.entrypoint(service, pool_size=1) as loop:
        logger.info(f'Initialize database: {config.db}')
        loop.run_until_complete(pg.init(
            host=config.db.host,
            port=config.db.port,
            database=config.db.database,
            user=config.db.user,
            password=config.db.password,
        ))
        loop.run_forever()


if __name__ == '__main__':
    main()
