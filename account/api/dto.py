from dataclasses import dataclass, field
from typing import Optional

import marshmallow.validate


@dataclass
class Status:
    status: int = field(metadata=dict(
        example=200,
        description='HTTP status code of current operation',
        validate=marshmallow.validate.Range(min=100, max=599)
    ))
    result: bool = field(metadata=dict(
        description='Result of current operation'
    ))
    addition: dict = field(default_factory=dict, metadata=dict(
        description='Object to represent current operation'
    ))
    description: Optional[str] = field(default=None, metadata=dict(
        description='Additional description to current operation'
    ))

    @staticmethod
    def success(
        addition: Optional[dict] = None,
        description: Optional[str] = None
    ) -> 'Status':
        return Status(
            status=200,
            result=True,
            addition=addition or dict(),
            description=description,
        )

    @staticmethod
    def fail(
        status: int = 400,
        addition: Optional[dict] = None,
        description: Optional[str] = None
    ) -> 'Status':
        return Status(
            status=status,
            result=False,
            addition=addition or dict(),
            description=description,
        )
