import asyncio
import logging

from aiomisc.service.periodic import PeriodicService
from asyncpgsa import PG
from random import shuffle

from account.db.schema import accounts_table as accounts_t
from account.db.util import accounts_count

logger = logging.getLogger(__name__)


class AccountBalanceCollector(PeriodicService):
    # Знаем, что никто кроме нас не будет брать блокирвку
    # с таким id ресурса в базе
    MAGIC_ID = 1337

    def __init__(
        self, pg: PG,
        max_rows_count: int = 1000,
        max_size: int = 2,
        estimate_counting: bool = True,
        *args, **kwargs
    ):
        """
        :param pg: pg singleton instance
        :param max_rows_count: max count of rows to process in one subtract
        :param max_size: maximum number of simultaneous tasks in `JobQueue`
        """
        super().__init__(*args, **kwargs)
        self.pg = pg
        self.max_rows_count = max_rows_count
        self.max_size = max_size
        self.estimate_counting = estimate_counting
        self._last_run_result = None

    @property
    def last_run_result(self):
        """
        Main goal is testing

        If last_run_result == False then only reason is trying
        start collector process while another exists
        Otherwise, it's Ok
        """
        return self._last_run_result

    async def callback(self):
        logger.info('Collector start updating')
        waiter_event = asyncio.Event()
        start_event = asyncio.Event()
        end_event = asyncio.Event()
        semaphore = asyncio.Semaphore(self.max_size)

        async def waiter():
            logger.info('Create global transaction for lock holding')
            # глобальная транзакция для удержания лока
            async with self.pg.transaction() as conn:
                logger.info('Collector try acquire lock')
                try_lock = await conn.fetchval(f'SELECT pg_try_advisory_xact_lock({self.MAGIC_ID});')
                if not try_lock:
                    logger.warning('Fail: lock has been already acquired')
                    end_event.set()
                    return
                logger.info('Success: collector acquired lock')
                start_event.set()
                await waiter_event.wait()

        async def runner(*boundaries):
            # только ограниченное число будет производить вычитание
            async with semaphore:
                logger.info(f'Got semaphore for range({boundaries[0], boundaries[1]})')
                await self._subtract_hold(*boundaries)

        can_start = asyncio.create_task(start_event.wait())
        need_end = asyncio.create_task(end_event.wait())
        waiter_task = asyncio.create_task(waiter())

        done, _ = await asyncio.wait([can_start, need_end], return_when=asyncio.FIRST_COMPLETED)
        if done.pop() is need_end:
            logger.warning('Collector has already run!')
            # Не забываем завершить таски
            can_start.cancel()
            waiter_task.cancel()
            self._last_run_result = False
            return

        # Не забываем завершить таски
        need_end.cancel()
        logger.info('Collector begin subtracting')

        # получим количество элементов и разобьем на участки обновления
        limit = await accounts_count(self.pg, estimated=self.estimate_counting)
        # запустим обновление таблицы
        await asyncio.gather(*[runner(*bs) for bs in self._make_boundaries(limit)])

        logger.info('Collector release pg advisory lock')
        # Завершим транзакцию, установив событие
        waiter_event.set()
        # дождемся освобождения лока
        await waiter_task
        self._last_run_result = True
        logger.info('Collector end work')

    def _make_boundaries(self, limit):
        """
        :returns: return lower bound, upper bound pairs to subtract rangeы
        """

        def split():
            iterations = limit // self.max_rows_count
            if limit % self.max_rows_count != 0:
                iterations += 1
            for i in range(iterations):
                yield i * self.max_rows_count + 1, (i + 1) * self.max_rows_count

        boundaries = list(split())
        shuffle(boundaries)

        logger.debug(f'All boundaries are computed - {boundaries}')
        return boundaries

    async def _subtract_hold(self, lower_bound: int, upper_bound: int):
        query = (accounts_t.update()
                 .where(accounts_t.c.id.between(lower_bound, upper_bound))
                 .where(accounts_t.c.closed.is_(False))
                 .values(
                    balance=accounts_t.c.balance - accounts_t.c.hold,
                    hold=0
                ))
        logger.info(f'Start balance subtracting in range({lower_bound}, {upper_bound})')
        await self.pg.fetchrow(query)
        logger.info(f'End balance subtracting in range({lower_bound}, {upper_bound})')
