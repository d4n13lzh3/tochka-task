from sqlalchemy import Boolean, Column, Integer, MetaData, Table, String
from sqlalchemy.dialects import postgresql as pg


metadata = MetaData()

# Здесь можно привести ко второй нормальной форме
# Используем постгресовский тип данных UUID
accounts_table = Table(
    'accounts', metadata,
    # Создаем serial поле
    Column('id', Integer, primary_key=True),
    # Создаем уникальный индекс
    Column('uid', pg.UUID, unique=True, index=True),
    Column('fullname', String, nullable=False),
    Column('balance', Integer, server_default='0', nullable=False),
    Column('hold', Integer, server_default='0', nullable=False),
    Column('closed', Boolean, server_default='true', nullable=False),
)
