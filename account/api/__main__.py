from aiomisc.log import basic_config

from account.api.app import Web


def setup_logging():
    basic_config(buffered=False)


async def app():
    setup_logging()
    return Web().web_app


def main():
    setup_logging()
    Web().run()
