import environ
import os

from account.settings.secret import SecretStr

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"


@environ.config
class DB:
    host = environ.var(converter=str)
    port = environ.var(converter=int)
    user = environ.var(converter=str)
    password = environ.var(converter=SecretStr)
    database = environ.var(converter=str)

    def format_dsn(self) -> str:
        return DSN.format(
            user=self.user,
            password=self.password,
            host=self.host,
            port=self.port,
            database=self.database,
        )


@environ.config
class Server:
    host = environ.var(default='0.0.0.0', converter=str)
    port = environ.var(default=8080, converter=int)


@environ.config(prefix='')
class AppConfig:
    db = environ.group(DB)
    server = environ.group(Server)

    debug = environ.var(default=False, converter=bool)


@environ.config(prefix='COLLECTOR')
class CollectorConfig:
    db = environ.group(DB)

    interval = environ.var(default=10 * 60 * 60, converter=int)
    delay = environ.var(default=0, converter=int)


def load_config() -> AppConfig:
    return environ.to_config(AppConfig)


def load_collector_config() -> CollectorConfig:
    db_options = dict()
    for env, val in os.environ.items():
        if env.startswith('DB'):
            db_options[f'COLLECTOR_{env}'] = val

    return environ.to_config(CollectorConfig, environ={
        **os.environ,
        **db_options,
    })
