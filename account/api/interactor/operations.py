import logging

from aiomisc import asyncbackoff
from asyncpgsa import PG
from asyncpg import PostgresError, Record
from typing import Callable, Coroutine, Optional, Tuple
from uuid import UUID

from account.api.dto import Status
from account.db.schema import accounts_table as accounts_t


AsyncFunction = Callable[..., Coroutine]

logger = logging.getLogger(__name__)
_backoff_cfg = dict(
    attempt_timeout=None,
    deadline=None,
    pause=0.1,
    max_tries=3,
    exceptions=[PostgresError]
)


def _log_status(function: AsyncFunction):
    async def wrapper(*args, **kwargs):
        """
        Log result of operation
        """
        status: Status = await function(*args, **kwargs)
        if status.result:
            logger.info(status.description)
        else:
            logger.warning(status.description)
        return status

    return wrapper


class OperationsInteractor:
    def __init__(self, pg: PG):
        self.pg = pg

    @_log_status
    @asyncbackoff(**_backoff_cfg)
    async def add(self, uid: UUID, amount: int) -> Status:
        """
        :param uid: unique number of user
        :param amount: amount of money expressed in kopecks
        :returns: status of operation
        """
        async with self.pg.transaction() as conn:
            account, error_status = await self._prefetch_account(uid, conn)
            if error_status is not None:
                return error_status

            await conn.fetchrow(self._update_query(
                uid=uid,
                balance=accounts_t.c.balance + amount
            ))
            account['balance'] += amount

            return Status.success(
                addition=account,
                description=f'Account {uid}: Add operation has been successfully completed'
            )

    @_log_status
    @asyncbackoff(**_backoff_cfg)
    async def subtract(self, uid: UUID, amount: int) -> Status:
        """
        :param uid: unique number of user
        :param amount: amount of money expressed in kopecks
        :returns: status of operation
        """
        async with self.pg.transaction() as conn:
            account, error_status = await self._prefetch_account(uid, conn)
            if error_status is not None:
                return error_status

            if account['balance'] <= account['hold'] + amount:
                return Status.fail(
                    addition=account,
                    description=(f'Account {uid} does not have '
                                 f'enough balance to complete operation'),
                )

            await conn.fetchrow(self._update_query(
                uid=uid,
                hold=accounts_t.c.hold + amount
            ))
            account['hold'] += amount

            return Status.success(
                addition=account,
                description=f'Account {uid}: Subtract operation has been successfully completed',
            )

    @_log_status
    @asyncbackoff(**_backoff_cfg)
    async def status(self, uid: UUID) -> Status:
        """
        :param uid: unique number of user
        :returns: status of operation
        """
        account: Optional[Record] = await self.pg.fetchrow(
            query=self._select_query(uid=uid)
        )
        return Status(
            status=200,
            result=account is not None,
            addition=dict(account or tuple()),
            description=f'Account {uid} {"not" if account is None else ""} found',
        )

    async def _prefetch_account(self, uid: UUID, conn) -> Tuple[Optional[dict], Optional[Status]]:
        """
        Method for prefetch account
        :param conn: connection class
        :returns: pair account, status. If account was found, status is None
        Otherwise account is None and status describes error
        If account is not None then it will be converted to dict
        """
        account = await conn.fetchrow(self._select_query(uid))
        if account is None:
            return None, Status.fail(
                status=404,
                description=f'Account with {uid} not found'
            )

        account = dict(account)
        if account['closed']:
            return None, Status.fail(
                addition=account,
                description=f'{account["fullname"]} has closed account',
            )

        return account, None

    @staticmethod
    def _select_query(uid: UUID):
        return accounts_t.select().where(accounts_t.c.uid == uid)

    @staticmethod
    def _update_query(uid: UUID, **values):
        return (accounts_t.update()
                .where(accounts_t.c.uid == uid)
                .values(**values))
