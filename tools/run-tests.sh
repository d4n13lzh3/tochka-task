#!/bin/bash
set -e

DIR=$(dirname "${BASH_SOURCE[0]}")

# shellcheck source=./load-env.sh
source "$DIR/load-env.sh"

cd "$DIR/../account/tests"
pytest "$@"
