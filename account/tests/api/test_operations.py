import asyncio
import pytest

from asyncpgsa import pg
from uuid import UUID

from account.api.dto import Status
from account.db.util import insert_accounts


pytestmark = pytest.mark.asyncio


async def test_concurrent_operations_on_account(operations):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 300, 100, False),
    ])
    # Запустим одновременно две операции:
    # - первая не должна выполниться, так как недостаточно средств
    # - вторая - выполнится
    # на выходе будет только пополнение счета
    status_1, status_2 = await asyncio.gather(
        operations.subtract('26c940a1-7228-4ea2-a3bc-e6460b172040', 300),
        operations.add('26c940a1-7228-4ea2-a3bc-e6460b172040', 300),
    )
    assert status_1.result is False
    assert status_2.result is True

    status = await operations.status('26c940a1-7228-4ea2-a3bc-e6460b172040')
    expected = Status(200, True, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 600,
        'hold': 100,
        'closed': False
    })
    assert_status(expected, status)


@pytest.mark.parametrize('operation', ['add', 'subtract'])
async def test_operation_not_permitted_closed_account(operations, operation):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 1700, 400, True),
    ])
    status = await getattr(operations, operation)(
        uid='26c940a1-7228-4ea2-a3bc-e6460b172040',
        amount=300,
    )
    expected = Status(400, False, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 1700,
        'hold': 400,
        'closed': True
    })
    assert_status(expected, status)


@pytest.mark.parametrize('operation', ['add', 'subtract'])
async def test_operation_not_permitted_account_not_found(operations, operation):
    status = await getattr(operations, operation)(
        uid='26c940a1-7228-4ea2-a3bc-e6460b172040',
        amount=300
    )
    expected = Status.fail(404)
    assert_status(expected, status)


async def test_subtract(operations):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 1700, 400, False),
    ])
    status = await operations.subtract(
        uid='26c940a1-7228-4ea2-a3bc-e6460b172040',
        amount=300
    )
    expected = Status(200, True, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 1700,
        'hold': 700,
        'closed': False
    })
    assert_status(expected, status)
    expected_balance = await pg.fetchval(
        'SELECT hold FROM accounts WHERE uid=\'26c940a1-7228-4ea2-a3bc-e6460b172040\';'
    )
    assert expected_balance == 700


async def test_subtract_not_enough_money(operations):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 100, 100, False),
    ])
    status = await operations.subtract(
        uid='26c940a1-7228-4ea2-a3bc-e6460b172040',
        amount=300
    )
    expected = Status(400, False, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 100,
        'hold': 100,
        'closed': False
    })
    assert_status(expected, status)
    expected_balance = await pg.fetchval(
        'SELECT hold FROM accounts WHERE uid=\'26c940a1-7228-4ea2-a3bc-e6460b172040\';'
    )
    assert expected_balance == 100


async def test_add(operations):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 1700, 300, False),
    ])
    status = await operations.add(
        uid='26c940a1-7228-4ea2-a3bc-e6460b172040',
        amount=300
    )
    expected = Status(200, True, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 2000,
        'hold': 300,
        'closed': False
    })
    assert_status(expected, status)
    expected_balance = await pg.fetchval(
        'SELECT balance FROM accounts WHERE uid=\'26c940a1-7228-4ea2-a3bc-e6460b172040\';'
    )
    assert expected_balance == 2000


async def test_status(operations):
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 1700, 300, False),
    ])
    status = await operations.status('26c940a1-7228-4ea2-a3bc-e6460b172040')
    expected = Status(200, True, {
        'id': 1,
        'uid': UUID('26c940a1-7228-4ea2-a3bc-e6460b172040'),
        'fullname': 'Петров Иван Сергеевич',
        'balance': 1700,
        'hold': 300,
        'closed': False
    })
    assert_status(expected, status)


async def test_status_not_found(operations):
    status = await operations.status('26c940a1-7228-4ea2-a3bc-e6460b172040')
    expected = Status(status=200, result=False)
    assert_status(expected, status)


def assert_status(expected_status: Status, actual_status: Status):
    assert expected_status.status == actual_status.status
    assert expected_status.addition == actual_status.addition
    assert expected_status.result == actual_status.result
