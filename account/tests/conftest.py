import os
import pytest

from asyncpgsa import pg
from sqlalchemy import create_engine
from sqlalchemy.pool import StaticPool
from pytest_postgresql.janitor import DatabaseJanitor

from account.db.schema import metadata
from account.settings.config import load_config
from account.api.interactor.operations import OperationsInteractor


@pytest.fixture()
def operations():
    return OperationsInteractor(pg)


@pytest.fixture()
def config():
    default_config = {
        'DB_HOST': os.environ.get('DB_HOST', 'localhost'),
        'DB_PORT': os.environ.get('DB_PORT', '5432'),
        'DB_DATABASE': 'test_db',
        'DB_USER': os.environ.get('DB_USER', 'example'),
        'DB_PASSWORD': os.environ.get('DB_PASSWORD', 'example'),
    }
    os.environ.update(default_config)
    return load_config()


@pytest.fixture()
def model_creator(config):
    def create():
        engine = create_engine(
            config.db.format_dsn(),
            poolclass=StaticPool,
            echo=False,
        )
        metadata.create_all(engine)
        return engine

    return create


@pytest.fixture(autouse=True)
def janitor(config, model_creator, event_loop):
    dict_config = dict(
        host=config.db.host,
        port=config.db.port,
        user=config.db.user,
        password=config.db.password,
    )
    # Не подсоединяемся к конкретной базе и запрашиваем версию
    event_loop.run_until_complete(pg.init(**dict_config))
    version = event_loop.run_until_complete(pg.fetchval('SELECT version();'))
    version = version.split(' ')[1]

    janitor = DatabaseJanitor(
        db_name=config.db.database,
        **dict_config,
        version=version,
    )
    janitor.init()
    # Создадим все модели
    sa_engine = model_creator()
    # Переинициализруемся для соединения с конкретной базой
    event_loop.run_until_complete(pg.init(
        database=config.db.database,
        **dict_config,
    ))
    yield
    sa_engine.dispose()
    janitor.drop()
