#!/bin/bash

DIR=$(dirname "${BASH_SOURCE[0]}")
echo "Loading env variables"
# shellcheck source=./load-env.sh
source "$DIR/load-env.sh"
docker-compose up -d --build
echo "Start up dockers"

echo "Trying update DB schemas..."
cd "$DIR/../account" || exit
attempts=3
# shellcheck disable=SC2050
while [ $attempts -ge 0 ]; do
  code=0
  for container_id in $(docker ps -aq -f name=^tochka-task_accounts_\\d+$); do
    # shellcheck disable=SC2086
    if ! docker exec -it $container_id alembic upgrade head; then
      code=1
      break
    fi
  done
  if [ $code -eq 0 ]; then
    break
  fi
  echo "Failed to update DB schemas. Sleep for 3s..."
  attempts=$((attempts - 1))
  sleep 3
done
echo "DB schemas updated"
