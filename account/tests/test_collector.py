import asyncio
import pytest

from asyncpg import Record
from asyncpgsa import pg
from collections import Counter

from account.collector import AccountBalanceCollector
from account.db.util import insert_accounts


pytestmark = pytest.mark.asyncio


async def test_collector_once():
    collector = AccountBalanceCollector(
        pg=pg,
        max_rows_count=2,
        max_size=2,
        interval=200,
        delay=0,
        # Сейчас не нужно получать оценку кол-ва, так как
        # только что создали базу
        estimate_counting=False,
    )
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172041', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172042', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172043', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172044', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172045', 'Петров Иван Сергеевич', 1700, 400, True),
        ('26c940a1-7228-4ea2-a3bc-e6460b172046', 'Петров Иван Сергеевич', 1700, 400, False),
    ])
    await collector.start()
    # Подождем результаты
    await asyncio.sleep(0.1)
    await collector.periodic.task

    result = await pg.fetch('SELECT uid, balance, hold FROM accounts ORDER BY id')
    result = [t(r) for r in result]
    assert result == [
        ('26c940a1-7228-4ea2-a3bc-e6460b172041', 1300, 0),
        ('26c940a1-7228-4ea2-a3bc-e6460b172042', 1300, 0),
        ('26c940a1-7228-4ea2-a3bc-e6460b172043', 1300, 0),
        ('26c940a1-7228-4ea2-a3bc-e6460b172044', 1300, 0),
        ('26c940a1-7228-4ea2-a3bc-e6460b172045', 1700, 400),  # закрытый счет, изменений не должно быть
        ('26c940a1-7228-4ea2-a3bc-e6460b172046', 1300, 0),
    ]

    await collector.stop(None)


async def test_collector_fail_on_concurrent():
    collector_factory = lambda: AccountBalanceCollector(
        pg=pg,
        max_rows_count=2,
        max_size=2,
        interval=200,
        delay=0,
        # Сейчас не нужно получать оценку кол-ва, так как
        # только что создали базу
        estimate_counting=False,
    )
    await insert_accounts(pg, [
        ('26c940a1-7228-4ea2-a3bc-e6460b172041', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172042', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172043', 'Петров Иван Сергеевич', 1700, 400, False),
        ('26c940a1-7228-4ea2-a3bc-e6460b172044', 'Петров Иван Сергеевич', 1700, 400, False),
    ])
    collector_1 = collector_factory()
    collector_2 = collector_factory()
    collector_3 = collector_factory()

    await collector_1.start(),
    await collector_2.start(),
    await collector_3.start()

    # Подождем результаты
    await asyncio.sleep(0.1)
    await asyncio.wait([
        collector_1.periodic.task,
        collector_2.periodic.task,
        collector_3.periodic.task,
    ])

    # Прямой порядок запуска не обеспечивается, поэтому просто убедимся,
    # что в двух случаях из трех, результат был отрицательный
    result = Counter()
    for collector in collector_1, collector_2, collector_3:
        result[collector.last_run_result] += 1
    assert result[True] == 1
    assert result[False] == 2

    await collector_1.stop(None)
    await collector_2.stop(None)
    await collector_3.stop(None)


def t(record: Record):
    return str(record['uid']), record['balance'], record['hold']
