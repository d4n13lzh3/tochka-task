#!/bin/bash

DIR=$(dirname "${BASH_SOURCE[0]}")
# shellcheck disable=SC2046
export $(grep -vE '^#' "$DIR/dev.env" | xargs)
