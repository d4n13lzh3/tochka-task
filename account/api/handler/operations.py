from aiohttp import web
from aiohttp_apispec import docs, json_schema, match_info_schema, response_schema
from dataclasses import asdict
from marshmallow import Schema, fields, validate
from marshmallow_dataclass import class_schema
from typing import Any, Union

from account.api.dto import Status
from account.api.interactor.operations import OperationsInteractor

StatusSchema = class_schema(Status)


class OperationPostRequestSchema(Schema):
    uid = fields.UUID(required=True, description='Unique user id')
    amount = fields.Integer(
        validate=validate.Range(min=0, min_inclusive=False),
        required=True,
        description='Amount of money expressed in kopecks'
    )


class OperationGetRequestSchema(Schema):
    uid = fields.UUID(
        required=True,
        description='Unique user id'
    )


class OperationsHandler:
    def __init__(self, interactor: OperationsInteractor):
        self._interactor = interactor

    @docs(
        tags=['operations'],
        summary='Add a sum to user balance',
        description='Add a sum to user balance',
    )
    @json_schema(OperationPostRequestSchema)
    @response_schema(StatusSchema)
    async def add(self, request):
        status = await self._interactor.add(**request['json'])
        status.addition = self._dump(status.addition)
        return web.json_response(asdict(status), status=status.status)

    @docs(
        tags=['operations'],
        summary='Subtract operation',
        description='Increase hold of account',
    )
    @json_schema(OperationPostRequestSchema)
    @response_schema(StatusSchema)
    async def subtract(self, request):
        status = await self._interactor.subtract(**request['json'])
        status.addition = self._dump(status.addition)
        return web.json_response(asdict(status), status=status.status)

    @docs(
        tags=['operations'],
        summary='Get status of account',
        description='Get status of account',
    )
    @match_info_schema(OperationGetRequestSchema)
    @response_schema(StatusSchema)
    async def status(self, request):
        status = await self._interactor.status(**request['match_info'])
        status.addition = self._dump(status.addition)
        return web.json_response(asdict(status), status=status.status)

    @classmethod
    def _dump(cls, object_: Any) -> Union[str, dict]:
        """
        Brute objects converter into serializable objects
        """
        try:
            dict_ = dict(object_)
            for k, v in dict_.items():
                dict_[k] = cls._dump(v)
            return dict_
        except (TypeError, ValueError):
            return str(object_)
