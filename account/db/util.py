from asyncpgsa import PG
from sqlalchemy import select, func
from typing import Iterable, Tuple
from uuid import UUID

from account.db.schema import accounts_table as accounts_t


async def accounts_count(pg: PG, estimated=True):
    """
    Count calculation of accounts
    If estimated = True return estimated count
    """
    if estimated:
        query = ('SELECT c.reltuples::bigint AS estimate '
                 'FROM   pg_class c '
                 'WHERE  c.relname = \'{}\' ;'.format(accounts_t.name, 'example'))
    else:
        query = select([func.count()]).select_from(accounts_t)
    return await pg.fetchval(query)


async def insert_accounts(pg: PG, values: Iterable[Tuple]):
    # вспомогательная функция для формирования записи
    # в ином случае tuple требовал бы наличие поля id,
    # которое должно формироваться автоматически в базе
    def d(t):
        uid = UUID(t[0]) if isinstance(t[0], str) else t[0]
        return {
            'uid': uid,
            'fullname': t[1],
            'balance': t[2],
            'hold': t[3],
            'closed': t[4],
        }

    query = accounts_t.insert().values([d(v) for v in values])
    await pg.fetchrow(query)
