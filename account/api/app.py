import logging

from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware
from asyncpgsa import pg
from dataclasses import asdict
from time import time

from account.api.dto import Status
from account.api.handler.operations import OperationsHandler
from account.api.interactor.operations import OperationsInteractor
from account.api.middlewares import fail_status_on_failure
from account.db.util import accounts_count, insert_accounts
from account.settings.config import load_config


logger = logging.getLogger(__name__)


class App:
    def __init__(self):
        logger.info('Load config from environments...')
        self.config = load_config()
        self.pg = pg
        self.interactor = OperationsInteractor(self.pg)

    async def init(self, _):
        logger.info(f'Initialize database: {self.config.db}')
        await self.pg.init(
            host=self.config.db.host,
            port=self.config.db.port,
            database=self.config.db.database,
            user=self.config.db.user,
            password=self.config.db.password,
        )

        if self.config.debug:
            await self._insert_samples()

    async def ping(self):
        t1 = time()
        await self.pg.query('SELECT 1;')
        return time() - t1

    async def _insert_samples(self):
        count = await accounts_count(self.pg, estimated=False)
        if count == 0:
            logger.info('Insert samples into accounts table')
            await insert_accounts(self.pg, [
                ('26c940a1-7228-4ea2-a3bc-e6460b172040', 'Петров Иван Сергеевич', 1700, 300, False),
                ('7badc8f8-65bc-449a-8cde-855234ac63e1', 'Kazitsky Jason', 200, 200, False),
                ('5597cc3d-c948-48a0-b711-393edf20d9c0', 'Пархоменко Антон Александрович', 10, 300, False),
                ('867f0924-a917-4711-939b-90b179a96392', 'Петечкин Петр Измаилович', 1000000, 1, True),
            ])


class Web:
    def __init__(self):
        self.app = App()
        self.web_app = web.Application(logger=logger)
        self.handler = OperationsHandler(self.app.interactor)

        self.web_app.router.add_get('/api/ping', self.ping)
        # allow_head=False не позволяет создать также метод HEAD для этого обработчика
        self.web_app.router.add_get('/api/status/{uid}', self.handler.status, allow_head=False)
        self.web_app.router.add_post('/api/add', self.handler.add)
        self.web_app.router.add_post('/api/subtract', self.handler.subtract)

        self.web_app.on_startup.append(self.app.init)

        self.web_app.middlewares.append(validation_middleware)
        self.web_app.middlewares.append(fail_status_on_failure)
        setup_aiohttp_apispec(
            app=self.web_app,
            title='Account Helper',
            version='v1',
            swagger_path='/api/docs',
        )

    async def ping(self, _):
        return web.json_response(
            asdict(Status.success(addition={
                'ping': await self.app.ping(),
                'description': 'Ping OK'
            }))
        )

    def run(self):
        logger.info('Starting web app...')
        web.run_app(
            app=self.web_app,
            host=self.app.config.server.host,
            port=self.app.config.server.port,
        )
