import os
from importlib.machinery import SourceFileLoader

from pkg_resources import parse_requirements
from setuptools import find_packages, setup

module_name = 'account'

module = SourceFileLoader(
    fullname=module_name,
    path=os.path.join(module_name, '__init__.py'),
).load_module()


def load_requirements(filename: str) -> list:
    with open(filename, 'r') as fp:
        for req in parse_requirements(fp.read()):
            extras = '[{}]'.format(','.join(req.extras)) if req.extras else ''
            yield '{}{}{}'.format(req.name, extras, req.specifier)


setup(
    name=module_name,
    version='0.0.1',
    author='d4n13lzh3',
    author_email='zhe.dan28@gmail.com',
    long_description=open('README.md').read(),
    platforms='all',
    classifiers=[
        'Intended Audience :: Developers',
        'Natural Language :: Russian',
        'Operating System :: MacOS',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython'
    ],
    python_requires='>=3.7',
    packages=find_packages(exclude=['tests']),
    install_requires=load_requirements('requirements.txt'),
    extras_require={'dev': load_requirements('requirements.dev.txt')},
    entry_points={
        'console_scripts': [
            '{0}-api = {0}.api.__main__:main'.format(module_name),
            '{0}-collector = {0}.collector.__main__:main'.format(module_name)
        ]
    },
    include_package_data=True
)
