import logging

from aiohttp import web

from account.api.dto import Status

logger = logging.getLogger(__name__)


@web.middleware
async def fail_status_on_failure(request, handler):
    try:
        return await handler(request)
    except web.HTTPException as ex:
        logger.exception(ex)
        return Status.fail(
            status=ex.status,
            description='Opss... Something went wrong'
        )
    except Exception as ex:
        logger.exception(ex)
        return Status.fail(
            status=500,
            description='Opss... Something went wrong'
        )
