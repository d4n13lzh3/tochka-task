FROM python:3

ARG supervisor_version=4.1.0
ARG work_dir=/account
WORKDIR ${work_dir}

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install supervisor==${supervisor_version}

RUN mkdir -p /etc/supervisor/conf.d
RUN mkdir -p /var/log/supervisor

COPY account .
COPY gunicorn_conf.py .

COPY docker/supervisor/supervisord.conf /etc/supervisor/
COPY docker/supervisor/account-*.conf /etc/supervisor/conf.d/
COPY docker/bin/* /bin/


RUN chmod -R +x /bin/
ENV PYTHONPATH /

CMD /bin/start.sh
