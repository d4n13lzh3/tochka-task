#!/bin/bash

printf "READY\n"

while read -r line; do
  echo "Processing Event: $line" >&2;
  kill -SIGKILL $PPID
done < /dev/stdin
